import React, { Component } from 'react';
import thunk from 'redux-thunk';
import { createStore, applyMiddleware } from 'redux';
import { Provider } from 'react-redux';
import { StyleSheet, Text, View } from 'react-native';
import { createStackNavigator } from 'react-navigation';
import reducers from './src/reducers';
import Tabs from './src/navigators/booking-tabs';
import NavigationService from './src/utils/navigation-service';

const preloadedState = {};
const store = createStore(reducers, preloadedState, applyMiddleware(thunk));
const Stack = createStackNavigator({
  Home: Tabs
}, { initialRouteName: 'Home' });

export default class App extends Component {

  render() {
    return (
      <Provider store={store}>
        <View style={styles.container}>
          <Stack
            ref={navigatorRef => { NavigationService.setTopLevelNavigator(navigatorRef) }}
          />
        </View>
      </Provider>
    );
  }
}

const styles = StyleSheet.create({
  container: {
    flex: 1,
    backgroundColor: '#fff',
  },
});
