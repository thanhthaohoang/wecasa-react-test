import { Dimensions } from 'react-native';
import { createMaterialTopTabNavigator } from 'react-navigation';
import { PrestationsListScreen, AddressSelectionScreen, AppointmentSelectionScreen } from '../screens';

const tabBarConfig = {
  initialLayout: {
    height: 0,
    width: Dimensions.get('window').width,
  },
  tabBarOptions: {
    activeTintColor: 'black',
    inactiveTintColor: 'grey',
    labelStyle: {
      fontSize: 10,
    },
    style: {
      backgroundColor: 'white',
    },
    indicatorStyle: {
      backgroundColor: '#f86c28',
    },
  },
};

const Tabs = createMaterialTopTabNavigator({
  PrestationsList: PrestationsListScreen,
  AddressSelection: AddressSelectionScreen,
  AppointmentSelection: AppointmentSelectionScreen,
}, { ...tabBarConfig, initialRouteName: 'PrestationsList' });

Tabs.navigationOptions = { title: 'Réservation' };

export default Tabs;
