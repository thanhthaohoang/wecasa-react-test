import { combineReducers } from 'redux'
import universeReducer from './universe/universe';
import basketReducer from './basket/basket';

export default combineReducers({
  universe: universeReducer,
  basket: basketReducer,
});
