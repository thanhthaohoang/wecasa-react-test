import constants from '../../constants/constants';

const initialState = {
  all: {},
  isLoading: false,
  isLoaded: false,
}

const universe = (state = initialState, action) => {
  switch(action.type) {
    case constants.UNIVERSE.START_FETCHING:
      return { ...state, isLoading: true };
    case constants.UNIVERSE.FETCHED:
      return { ...state, all: action.payload.data, isLoading: false, isLoaded: true };
    default:
      return state;
  }
};

export default universe;
