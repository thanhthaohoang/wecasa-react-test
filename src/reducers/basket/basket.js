import constants from '../../constants/constants';

const initialState = {
  allPrestations: [],
  address: '',
  appointment: '',
}

const basket = (state = initialState, action) => {
  switch(action.type) {
    case constants.BASKET.ADD :
      return { ...state, allPrestations: [...state.allPrestations, action.payload.prestation] };
    case constants.BASKET.REMOVE :
      return {
        ...state,
        allPrestations: state.allPrestations.filter(p => p.reference !== action.payload.prestation.reference),
      };
    case constants.BASKET.SELECT_ADDRESS :
      return { ...state, address: action.payload.address };
    case constants.BASKET.SELECT_APPOINTMENT :
      return { ...state, appointment: action.payload.appointment };
    case constants.BASKET.CLEAN :
      return { ...state, ...initialState };
    default :
      return state;
  }
};

export default basket;
