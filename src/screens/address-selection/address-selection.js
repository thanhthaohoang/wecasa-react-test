import React, { Component } from 'react';
import { View, Text, TextInput, Button, Alert } from 'react-native';
import { connect } from 'react-redux';
import actions from '../../actions';
import { bindActionCreators } from 'redux';
import { flatMap } from 'lodash';
import { Basket } from '../../components';
import style from   './style';

class AddressSelectionScreen extends Component {

  static navigationOptions = {
    title: 'Adresse',
  };

  constructor(props) {
    super(props);
    this.state = { text: '' };
  }

  selectAddress() {
    const { text } = this.state;
    if (text === '') {
      Alert.alert(
        'Erreur',
        'Veuillez entrez une adresse.',
        [{ text: 'Compris !', style: 'cancel' }]
      );
    } else {
      this.props.actions.selectAddress(this.state.text);
    }
  }

  render() {
    return (
      <View style={style.container}>
        <TextInput
          style={{ height: 40 }}
          placeholder={'Entrez votre adresse'}
          onChangeText={(text) => this.setState({ text })}
        />
        <Button
          onPress={() => this.selectAddress()}
          title={'Valider'}
        />
        <View style={style.container}>
          <Basket />
        </View>
      </View>
    );
  }
}

const mapStateToProps = (state) => {
  return { data: state.basket };
}

const mapDispatchToProps = (dispatch) => {
  return { actions: bindActionCreators(actions.basket, dispatch) };
};

export default connect(mapStateToProps, mapDispatchToProps)(AddressSelectionScreen);
