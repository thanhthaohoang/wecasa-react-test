import React, { Component } from 'react';
import { View, Text, FlatList, ActivityIndicator, TouchableOpacity } from 'react-native';
import { connect } from 'react-redux';
import actions from '../../actions';
import { bindActionCreators } from 'redux';
import { flatMap } from 'lodash';
import { Basket } from '../../components';
import style from './style';

class PrestationsListScreen extends Component {

  static navigationOptions = {
    title: 'Prestations',
  };

  componentDidMount() {
    this.props.actions.fetchUniverse();
  }

  renderPrestationItem(item) {
    return (
      <TouchableOpacity onPress={() => this.props.actions.addPrestation(item)}>
        <Text>{item.title}</Text>
      </TouchableOpacity>
    );

  }

  renderLoader() {
    return <ActivityIndicator size={'large'} />;
  }

  render() {
    const { data } = this.props;
    if (data.isLoading) return this.renderLoader();
    if (data.isLoaded) {
      const categories = data.all.categories.map(category => category.title);
      const prestations = flatMap(data.all.categories.map(category => category.prestations));

      return (
        <View style={style.container}>
          <Text style={style.text}>Cliquez sur votre prestation préférée pour l'ajouter au panier :</Text>
          <FlatList
            data={prestations}
            renderItem={({ item }) => this.renderPrestationItem(item)}
            keyExtractor={prestation => prestation.reference}
            ListFooterComponent={<Basket />}
          />
        </View>
      );
    } else return null;
  }
}

const mapStateToProps = (state) => {
  return { data: state.universe };
}

const mapDispatchToProps = (dispatch) => {
  return { actions: bindActionCreators({ ... actions.universe, ...actions.basket }, dispatch) };
};

export default connect(mapStateToProps, mapDispatchToProps)(PrestationsListScreen);
