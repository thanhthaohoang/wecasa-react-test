import React, { Component } from 'react';
import { View, Text, TextInput, Button, Alert, DatePickerIOS } from 'react-native';
import { connect } from 'react-redux';
import actions from '../../actions';
import { bindActionCreators } from 'redux';
import { flatMap } from 'lodash';
import moment from 'moment';
import { Basket } from '../../components';
import style from   './style';

class AppointmentSelectionScreen extends Component {

  static navigationOptions = {
    title: 'Date de rendez-vous',
  };

  constructor(props) {
    super(props);
    this.state = {
      chosenAppointment: new Date(),
    };
  }

  selectAppointment() {
    const FIRST_HOUR = 7;
    const LAST_HOUR = 22;
    const { chosenAppointment } = this.state;
    if (moment(chosenAppointment).isBefore(moment())) {
      Alert.alert(
        'Erreur',
        'Veuillez choisir une date de rendez-vous dans le futur.',
        [{ text: 'Compris !', style: 'cancel' }]
      );
    } else if (LAST_HOUR < moment(chosenAppointment).hours() || moment(chosenAppointment).hours() < FIRST_HOUR) {
      Alert.alert(
        'Erreur',
        'Veuillez choisir une heure de rendez-vous entre 7h et 22h.',
        [{ text: 'Compris !', style: 'cancel' }]
      );
    } else {
      this.props.actions.selectAppointment(chosenAppointment);
    }
  }

  render() {
    const { chosenAppointment } = this.state;

    return (
      <View style={style.container}>
        <Text>{moment(chosenAppointment).format('DD/MM/YYYY à HH[h]mm')}</Text>
        <DatePickerIOS
          date={chosenAppointment}
          onDateChange={(newDate) => this.setState({ chosenAppointment: newDate })}
        />
        <Button
          onPress={() => this.selectAppointment()}
          title={'Valider'}
        />
        <View style={style.container}>
          <Basket />
        </View>
      </View>
    );
  }
}

const mapStateToProps = (state) => {
  return { data: state.basket };
}

const mapDispatchToProps = (dispatch) => {
  return { actions: bindActionCreators(actions.basket, dispatch) };
};

export default connect(mapStateToProps, mapDispatchToProps)(AppointmentSelectionScreen);
