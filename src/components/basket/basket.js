import React, { Component } from 'react';
import { View, Text, Button } from 'react-native';
import { connect } from 'react-redux';
import { bindActionCreators } from 'redux';
import moment from 'moment';
import actions from '../../actions';
import style from './style';

class Basket extends Component {

  renderTotalDuration() {
    const { allPrestations } = this.props.data;
    const totalMinutes = allPrestations.reduce((acc, cur) => { return acc + cur.duration }, 0);
    return (
      totalMinutes < 60
        ? totalMinutes + 'minutes'
        : Math.floor(totalMinutes / 60) + 'h' + totalMinutes % 60
    );
  }

  renderTotalPrice() {
    const { allPrestations } = this.props.data;
    return allPrestations.reduce((acc, cur) => { return acc + cur.price / 100 }, 0) + '€';
  }

  render() {
    const { allPrestations, address, appointment } = this.props.data;

    return (
      <View style={style.container}>
        <Text style={style.text}>Votre panier</Text>
        {allPrestations.map((prestation, index) => {
          return (
            <View key={index} style={style.rowContainer}>
              <Text style={{ flex: 1 }}>{prestation.title}</Text>
              <Button
                onPress={() => this.props.actions.removePrestation(prestation)}
                title={'Supprimer'}
                color={'red'}
              />
            </View>
          );
        })}
        <View style={style.totalsContainer}>
          <Text>Adresse : {address}</Text>
          <Text>
            Date de rendez-vous : {appointment && moment(appointment).format('DD/MM/YYYY à HH[h]mm')}
          </Text>
          <Text>
            Prix total : {allPrestations.length > 0 && this.renderTotalPrice()}
          </Text>
          <Text>
            Durée total : {allPrestations.length > 0 && this.renderTotalDuration()}
          </Text>
        </View>
        {allPrestations.length > 0 && address !== '' && appointment &&
          <Button
            onPress={() => this.props.actions.confirmBooking()}
            title={'Confirmer'}
          />}
      </View>
    );
  }
}

const mapStateToProps = (state) => {
  return { data: state.basket };
}

const mapDispatchToProps = (dispatch) => {
  return { actions: bindActionCreators(actions.basket, dispatch) };
};

export default connect(mapStateToProps, mapDispatchToProps)(Basket);
