import * as universe from './universe/universe';
import * as basket from './basket/basket';

export default {
  universe,
  basket,
};
