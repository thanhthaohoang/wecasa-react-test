import constants from '../../constants/constants';

export const fetchUniverse = () => {
  return (dispatch) => {
    dispatch({ type: constants.UNIVERSE.START_FETCHING });
    return fetch('http://localhost:3000/api/techtest/universe')
      .then(response => response.json())
      .then(data => dispatch({ type: constants.UNIVERSE.FETCHED, payload: { data } }));
  }
};
