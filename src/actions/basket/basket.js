import constants from '../../constants/constants';
import NavigationService from '../../utils/navigation-service';

export const addPrestation = (prestation) => {
  return (dispatch) => {
    dispatch({ type: constants.BASKET.ADD, payload: { prestation } });
  }
};

export const removePrestation = (prestation) => {
  return dispatch => dispatch({ type: constants.BASKET.REMOVE, payload: { prestation } });
};

export const selectAddress = (address) => {
  return dispatch => dispatch({ type: constants.BASKET.SELECT_ADDRESS, payload: { address } });
};

export const selectAppointment = (appointment) => {
  return dispatch => dispatch({ type: constants.BASKET.SELECT_APPOINTMENT, payload: { appointment } });
};

export const confirmBooking = () => {
  return (dispatch, getState) => {
    const data = {
      prestations: getState().basket.allPrestations.map(p => p.reference),
      appointment: getState().basket.appointment,
      address: getState().basket.address,
    };
    const init = {
      headers: {
        'Accept': 'application/json',
        'Content-Type': 'application/json'
      },
      body: JSON.stringify(data),
      method: 'POST',
    };
    fetch('http://localhost:3000/api/techtest/booking', init)
      .then(response => response.json())
      .then(data => {
        if (data.success) {
          dispatch({ type: constants.BASKET.CLEAN });
          NavigationService.navigate('PrestationsList');
        }
      }).catch(err => console.log(err));
  }
}
